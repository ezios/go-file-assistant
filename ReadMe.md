# 文件传输助手

> author: `wangj`
> 
> version: `0.0.1-alpha`
> 
> date: `2024年4月6日`
>
>基于`tcp/ip`协议，支持多客户端同时连接，支持文件传输、文件接收、文件删除等操作。

## 编译步骤
```shell
$ go build
```

## 运行步骤
```shell
$ ./file-assistant
```

# 配置文件
参考`config.json`文件
```json
{
    "ip": "127.0.0.1", // 监听ip
    "port": 8888    // 监听端口
}
```
