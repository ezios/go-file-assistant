package core

import "encoding/json"

var MESSAGE_OK = 0
var MESSAGE_CONN = 200
var MESSAGE_FILE = 201
var MESSAGE_TERMINATE = 500

type Message struct {
	Code     int          `json:"code"`
	FileInfo TransferFile `json:"fileInfo"`
}

// 序列化为json数据
func (m *Message) ToJson() ([]byte, error) {
	return json.Marshal(m)
}

// 反序列化
func ParseFromJson(data []byte) Message {
	var m Message
	_ = json.Unmarshal(data, &m)
	return m
}

func ConnectionMessage() Message {
	return Message{Code: MESSAGE_CONN}
}
func FileMessage(fileInfo TransferFile) Message {
	return Message{Code: MESSAGE_FILE, FileInfo: fileInfo}
}
func OkMessage() Message {
	return Message{Code: MESSAGE_OK}
}
func TerminateMessage() Message {
	return Message{Code: MESSAGE_TERMINATE}
}
