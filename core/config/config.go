package config

import (
	"encoding/json"
	"os"
)

type Config struct {
	Ip   string
	Port int
}

func Load() Config{
	data , err :=os.ReadFile("config.json")
	if err != nil {
		println("读取配置文件失败")
		panic(err)
	}
	var config Config
	json.Unmarshal(data, &config)
	return config

}