package core

import (
	"os"
	"path/filepath"
)

type TransferFile struct {
	Path string `json:"path"`
	Name string `json:"name"`
	Size int64  `json:"size"`
	self *os.File
}

/*
* 加载文件信息
 */
func LoadFile(file *os.File) TransferFile {
	t := TransferFile{}
	t.self = file
	t.Name = filepath.Base(file.Name())
	t.Path = file.Name()
	stat, _ := file.Stat()
	t.Size = stat.Size()
	return t
}

/**
*	根据信息创建文件
 */
func (t *TransferFile) CreateFile() (err error) {
	t.self, err = os.Create(t.Path)
	return err
}

/*
* 读取文件数据
 */
func (t *TransferFile) Read(p []byte) (n int, err error) {
	return t.self.Read(p)
}

/**
* 写入文件数据
 */
func (t *TransferFile) Write(p []byte) (n int, err error) {
	return t.self.Write(p)
}

// 关闭文件
func (t *TransferFile) Close() (err error) {
	return t.self.Close()
}