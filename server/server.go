package server

import (
	"fmt"
	"net"
)

type Server struct {
	Host string
	Port int
	listener net.Listener
}

// 创建服务端
func NewServer(host string, port int) *Server {

	return &Server{
		Host: host,
		Port: port,
	}
}

// 启动服务器
func (s *Server) Connect() {
	local := net.TCPAddr{
		IP:   net.ParseIP(s.Host),
		Port: s.Port,
	}
	tcpListener, err := net.ListenTCP("tcp", &local)
	if err != nil {
		panic(err)
	}
	s.listener = tcpListener
}

func (s *Server) Accept() {
	fmt.Printf("[%s] 服务器开启监听。\n",s.listener.Addr())
	
	
	for{
		conn,_ := s.listener.Accept()
		connection := Connection{TcpConn: conn}

		go connection.work()
	}
}


