package main

import (
	"fmt"
	"net"
	"os"
	"path/filepath"
	"time"
)

func main() {
	conn, err := net.Dial("tcp", "127.0.0.1:8080")
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	file := input()
	defer file.Close()

	fileName := filepath.Base(file.Name())
	conn.Write([]byte(fileName))

	data := make([]byte, 1024)
	time.Sleep(300 * time.Microsecond)
	for {
		num, err := file.Read(data)
		if err != nil {
			if(err.Error() != "EOF"){
				fmt.Println(err.Error())
				break;
			}
			fmt.Println("文件发送完毕")
			break
		}
		conn.Write(data[:num])

	}

}

func input() *os.File {
	var filePath string
	var file *os.File
	fmt.Println("请输入文件路径：")
	fmt.Scan(&filePath)
	fmt.Println()
	file, err := os.Open(filePath)
	if err != nil {
		fmt.Println("文件不存在")
	}

	return file
}
