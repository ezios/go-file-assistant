package main

import (
	"fmt"
	"os"

	"gitee.com/ezios/file-assistant/client"
	"gitee.com/ezios/file-assistant/core"
)

func main() {
	client := client.NewClient("127.0.0.1", 8080)
	file:=input()
	// client.Connect()
	transferFile :=core.LoadFile(file)
	client.Work(&transferFile)
}

func input() *os.File {
	var filePath string
	var file *os.File
	fmt.Println("请输入文件路径：")
	fmt.Scan(&filePath)
	fmt.Println()
	file, err := os.Open(filePath)
	if err != nil {
		fmt.Println("文件不存在")
	}

	return file
}
