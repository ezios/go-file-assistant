package main

import "gitee.com/ezios/file-assistant/server"

func main() {
	server := server.NewServer("127.0.0.1", 8080)
	server.Connect()
	server.Accept()

}